﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Xml.Serialization;
using Motos;

namespace DemoSerialisation
{
	class DemoSerialisation
	{
		/// <summary>
		/// Sérialise une Moto vers un fichier xml<br/>
		/// Voir le projet VerySimpleDemoSerialisation.
		/// </summary>
		/// <param name="laMoto">L'objet Moto à sérialiser</param>
		/// <param name="fileName">le nom du fichier</param>
		public static void SerializeMotoToFile(Moto laMoto, string fileName)
		{
			//on creer notre serializer
			XmlSerializer serializer = new XmlSerializer(typeof(Moto));

			//on creer le fichier ou creer
			using (StreamWriter writer = new StreamWriter(fileName))
            {	
				//on serialize lamoto dans writer
				serializer.Serialize(writer, laMoto);
            }
		}

		/// <summary>
		/// Dé-sérialise une Moto depuis un fichier xml<br/>
		/// Voir le projet VerySimpleDemoSerialisation.
		/// </summary>
		/// <param name="fileName">Nom du fichier xml</param>
		/// <returns>L'objet Moto dé-sérialisé.</returns>
		public static Moto DeserializeMotoFromFile(string fileName)
		{
			Moto UneAutreMoto;
			XmlSerializer serializer = new XmlSerializer(typeof(Moto));
			using (Stream reader = new FileStream(fileName, FileMode.Open))
			{
				UneAutreMoto = serializer.Deserialize(reader) as Moto;
			}
			return UneAutreMoto;
        }

        /// <summary>
        /// Sérialise un objet Moto vers un flux TCP.
        /// Cette fonction met notre application en attente d'un client TCP. 
        /// Quand un client se connecte, la fonction lui renvoie un objet moto sérialisé.
        /// </summary>
        /// <param name="ipAddressServer">L'adresse IP du serveur.</param>
        /// <param name="port">Le n° de port.</param>
        /// <param name="laMoto">L'objet Moto à sérialiser.</param>
        public static void SerializeMotoToTcp(string ipAddressServer, int port, Moto laMoto)
		{
			XmlSerializer serializer = new XmlSerializer(typeof(Moto));
			IPAddress ip = IPAddress.Parse(ipAddressServer);
			TcpListener tcpListener = new TcpListener(ip, port);
			tcpListener.Start();
			TcpClient cli = tcpListener.AcceptTcpClient();
			using (NetworkStream ns = cli.GetStream())
			{
				serializer.Serialize(ns, laMoto);
			}
        }

        static void Main(string[] args)
		{
			// Les propriétaires de la moto.
			List<Personne> lesProp = new List<Personne>();
			lesProp.Add(new Personne { Nom = "Terrieur", Prenom="Alain", Id = 1 });
			lesProp.Add(new Personne { Nom = "Terrieur", Prenom = "Alex", Id = 2 });

			Moto MaMoto = new Moto { Marque = "Suzuki", 
										Model = "Gladius", 
										Categorie = TypeMine.MTT2, 
										Moteur = new Moteur { Cylindree = 650, 
															NbCylindres = 2, 
															Puissance = 72, 
															Reference="SFV" 
										},
										Proprietaires = lesProp
			};

			/// @TODO 
			/// Sérialisation / désérialisation vers/depuis un fichier avec les fonctions prévues
			try
            {
			SerializeMotoToFile(MaMoto, "test.xml");
			Console.WriteLine("serialisation faite");
            }
			catch(Exception e)
            {
			Console.WriteLine(e);
            }
			Console.WriteLine("\n");
			try
			{
				DeserializeMotoFromFile("test.xml");
				Console.WriteLine("deserialisation faite");
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}


			/// @TODO 
			/// Sérialisation vers le réseau 
			string ipAddressServer = "127.0.0.1";
			int port = 8000;
			try
            {
			SerializeMotoToTcp(ipAddressServer, port, MaMoto);
			Console.WriteLine("serialisation faite avec le reseau");
            }
			catch(Exception e)
            {
				Console.WriteLine(e);
            }

			Console.WriteLine("\n\t\tUNE TOUCHE");
			Console.ReadKey();
		}
	}
}
