﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using ConfigCollecteur;

namespace TestConfigurationApplication
{
	class TestConfigurationApplication
	{
		static void Main(string[] args)
		{
			try
			{
				//  Le nom du fichier de configuration de ce logiciel CollecteurEole
				string configFileName = "ConfigCollecteurEole.xml";

				//  L'objet de configuration et son sérialiser XML
				ConfigCollecteurEole configCollecteurEole = null;

				//  S'il existe un fichier de configuration, on l'utilise en dé-sérialisant vers un objet de configuration
				//  TO DO 4
				
				//  Pour les saisies claviers...
				string tmp;

				//  Saisie des informations de configuration du serveur ControlEole et du serveur Base de Données,
				//  ou valeurs par défaut si vide.
				Console.Write("Id ControlEole (Vide pour 3): ");
				tmp = Console.ReadLine();
				int idSite = (tmp != "") ? int.Parse(tmp) : 3;

				//  TO DO 1

				//  Instanciation de l'objet de configuration
				//  TO DO 2

				//  Sérialisation vers le fichier de configuration
				//  TO DO 3

				foreach (var site in configCollecteurEole.Sites.LesSites)
				{
					Console.WriteLine("Service ControlEole {3} depuis {0}:{1}//{2} ", site.Url, site.Port, site.ServicePath, site.Id);
				}
				Console.WriteLine("Modifiez \"ConfigCollecteurEole.xml\" pour ajouter d'autres services ControlEole à collecter.");

				//  Instanciation du CollecteurEole
				//  TO DO 6
			}
			catch (Exception ex)
			{
				Console.WriteLine("Oups..." + ex.Message);
			}
			finally
			{
				Console.Write("\n\nFINIR L'APPLICATION SVP");
				Console.ReadKey();
			}
		}
	}
}
