﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Motos;

namespace ClientDemoSerialisation
{
    class Program
    {
        public static Moto DeserializeMotoFromTcp(string ipAdress, int port)
        {
            Moto UneAutreMoto;
            XmlSerializer serializer = new XmlSerializer(typeof(Moto));
            TcpClient cli = new TcpClient(ipAdress, port);
            using (NetworkStream ns = cli.GetStream())
            {
                UneAutreMoto = serializer.Deserialize(ns) as Moto;
            }
            return UneAutreMoto;
        }
        static void Main(string[] args)
        {
            string ip = "127.0.0.1";
            int port = 8000;
            try
            {
                DeserializeMotoFromTcp(ip, port);
                Console.WriteLine("deserialisation faite");

            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            } 
            Console.ReadKey();
        }               
    }
}
