﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace VerySimpleDemoSerialisation
{
	/// <summary>
	/// La classe Moto sera sérialisée en Xml
	/// </summary>
	public class Moto
	{
		/// <summary>
		/// La marque.
		/// </summary>
		public string Marque { get; set; }

		/// <summary>
		/// Le modèle
		/// </summary>
		public string Model { get; set; }

		/// <summary>
		/// Mise en string pour visualisation simple.
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			string msg = Marque + " " + Model;
			return msg;
		}
	}

	class VerySimpleDemoSerialisation
	{
		static void Main(string[] args)
		{
			// Instanciation de la moto qu'on souhaite sérialiser.
			Moto MaMoto = new Moto { Marque = "Suzuki", Model = "Gladius" };

			Console.WriteLine("Sérialisation de " + MaMoto);

			// XmlSerializer est une classe permettant la sérialisation et la dé-sérialisation d'un objet vers un flux Xml.
			// Il est possible de contrôler le format Xml de sérialisation.
			// https://msdn.microsoft.com/library/system.xml.serialization.xmlserializer(v=vs.100).aspx
			XmlSerializer serializer = new XmlSerializer(typeof(Moto));

			// Un StreamWriter permet d'écrire une série de caractères séquentiels dans un flux, ici le fichier Moto.xml.
			// https://msdn.microsoft.com/library/system.io.streamwriter(v=vs.100).aspx
			using (StreamWriter writer = new StreamWriter("Moto.xml"))
			{
				serializer.Serialize(writer, MaMoto);
			}

			// Déréférence l'objet créé plus tôt et appelle le Garbage Collector pour nettoyer la mémoire...
			// Il s'agit juste de préciser que la dé-sérialisation qui sera effectuée ensuite créera effectivement un nouvel objet...
            // et accessoirement de vous montrer comment utiliser le GC...
			MaMoto = null;
			GC.Collect();
			GC.WaitForPendingFinalizers();

			// Le handle qui référencera l'objet dé-sérialisé
			Moto UneAutreMoto;
			// Un StreamReader permet de lire une série séquentielle de caractères à partir d'un flux d'octets.
			// https://msdn.microsoft.com/fr-fr/library/system.io.streamreader(v=vs.100).aspx
			using (StreamReader reader = new StreamReader("Moto.xml"))
			{
				UneAutreMoto = serializer.Deserialize(reader) as Moto;
			}
			Console.WriteLine("Dé-sérialisation vers " + UneAutreMoto);

			Console.WriteLine("\n\t\tUNE TOUCHE");
			Console.ReadKey();
		}
	}
}
