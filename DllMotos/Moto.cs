﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace Motos
{

	public enum TypeMine
	{
		MTT1 = 1,
		MTT2 = 2,
	}

	public class Moteur
	{
		[XmlAttribute("Cyl")]
		public uint Cylindree { get; set; }

		[XmlAttribute("NbCyl")]
		public uint NbCylindres { get; set; }

		[XmlElement("RéfMoteur")]
		public string Reference { get; set; }

		[XmlAttribute("Puis")]
		public uint Puissance { get; set; }

	}

	public class Personne
	{
		[XmlAttribute("Id")]
		public int Id { get; set; }

		[XmlElement("Nom")]
		public string Nom { get; set; }

		[XmlElement("Prénom")]
		public string Prenom { get; set; }
	}

	[XmlRoot("Moto")]
	public class Moto
	{
		[XmlElement("Marque")]
		public string Marque { get; set; }

		[XmlElement("Model")]
		public string Model { get; set; }

		[XmlElement("Catégorie")]
		public TypeMine Categorie { get; set; }

		[XmlElement("Moteur")]
		public Moteur Moteur { get; set; }

		// Dé-commentez et commentez les deux lignes suivantes pour voir la différence...
		//[XmlElement("Personne")]	 
		[XmlArray("Propriétaires")]
		[XmlArrayItem("Personne")]
		public List<Personne> Proprietaires { get; set; }

		public override string ToString()
		{
			string msg = Marque + " " + Model + " " + Categorie;
			return msg;
		}
	}
}